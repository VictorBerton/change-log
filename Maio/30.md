# 30/05 #

##Ozeias - Status iOS##

• Criação da célula de post de imagem no chat;

• Criação da célula de post de video no chat;

• Adição de novas cores na edição do moment;

• Adição de novas fonts na edição do moment;

• Correção “Mensagens são dadas como lida ao clicar em push do chat que não possui ação”;

• Correção “Recusar solicitação para Seguir faz com que o Botão de aceitar desapareça de outro usuário”:

• Correção “Opções para concluir moment aparecem junto com as opções de desenhar”;

• Correção “Texto fica á frente de botões durante edição em um Moment”;

• Correção “Indicar qual tipo de chamada estou recebendo”;

• Build 358 PRD.

* [Pipefy](https://app.pipefy.com/pipes/433202)


##Guilherme - Status Back##

• Problema de pushs duplicadas;

• Problema das marcações 


##Pedro - Status Back##

• Auxilio no deploy do ZUK em homologação

• Finalização do ambiente Docker usando Amazon ECS e Fargate para o Kurento one2many

• Configuração do Kurento com TURN STUN

• Deploy do Status em produção

• Testes com o Kurento


##Tobias - Status Android##

• 769 - Moments implementado;

• 835 - Deletar/Adicionar interesse faz busca por Local Específico não funcionar;

• 847 - Mostrar ao usuário qual cor está utilizando no desenho em um Moment;

• 854 - ícone exibindo a cor selecionada desaparece ao selecionar a mesma cor 2 vezes seguidas;


##Heitor - Status Android##

• 769 - Moments implementado;

• 850 - Ver mais continua aparecendo em postagens sem descrição;

• 856 - Nome completo muito grande pode sobrepor tempo de post;

• 857 - Ver Mais em post de usuário específico aparece sem necessidade;